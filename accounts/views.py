from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from .forms import LoginForm, SignUpForm


# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = AuthenticationForm(request.POST)
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect("projects")
    else:
        form = LoginForm()

    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)

        if request.POST.get("password") == request.POST.get(
            "password_confirmation"
        ):
            if form.is_valid():
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]
                user = User.objects.create_user(
                    username=username, password=password
                )

                if user is not None:
                    login(request, user)
                    return redirect("list_projects")
                else:
                    pass
            else:
                form.add_error(
                    "password_confirmation", "Passwords do not match"
                )
    else:
        form = SignUpForm()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
