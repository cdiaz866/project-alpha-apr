from django.urls import path
from .views import list_projects, show_project, create_project


urlpatterns = [
    path("projects/", list_projects, name="list_projects"),
    path("projects/<int:project_id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
