from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects.html", context)


@login_required
def show_project(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    context = {"project": project}
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            new_project = form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}

    return render(request, "projects/create_project.html", context)
